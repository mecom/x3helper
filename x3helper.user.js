// ==UserScript==
// @name         X3Helper
// @namespace    http://tampermonkey.net/
// @version      0.5
// @updateURL    https://gitlab.com/mecom/x3helper/raw/master/x3helper.user.js
// @downloadURL  https://gitlab.com/mecom/x3helper/raw/master/x3helper.user.js
// @description  Helper interno sagex3
// @author       Meska
// @match        *://sagex3:8124/*
// @match        *://192.168.2.8:8124/*
// @run-at       document-end
// @grant        GM_getResourceText
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// @grant        GM_getResourceURL
// @grant        GM_xmlhttpRequest
// ==/UserScript==

function aiuti() {
    $("input[type='checkbox'][data-s-selected='false']").click();
    // alert('test');
    //$("input[type='checkbox'][data-s-selected='false']").click()
}
function flagall() {
    var testFlag = setInterval(function(){

        if ($("input[type='checkbox'][data-s-selected='false']").length <= 1) {
            clearInterval(testFlag);
        }
        $("input[type='checkbox'][data-s-selected='false']").click();
    },250);
    // alert('test');
    //$("input[type='checkbox'][data-s-selected='false']").click()
}
function cancella(rowid) {
    var bloccato = $("a.s-mn-tools-link.s-mn-icon[style='background-image: url(\"/syracuse-ui/themes/desktop/images/x3iconsmenu/279.png\");']").length;
    var noncancellabile = $("a.s-mn-main-link[title='Elimina'][disabled='true']").length;
    if (bloccato === 1) {
        $("tr.s-grid-row")[rowid].click();
        setTimeout(function(){
            cancella(rowid+1);
        },500);
    } else if (noncancellabile === 1) {
        $("tr.s-grid-row")[rowid].click();
        setTimeout(function(){
            cancella(rowid+1);
        },500);
    } else {
        $("a.s-mn-main-link[title='Elimina']")[0].click();
        setTimeout(function(){
            var testSiCount = 0;
            var testSi = setInterval(function(){
                if ($("a.s-msgbox-buttons-link[title='Sì']").length > 0) {
                    clearInterval(testSi);
                    $("a.s-msgbox-buttons-link[title='Sì']")[0].click();
                    setTimeout(function(){
                        $("a.s-mn-tools-link[title='Aggiorna ....']")[0].click()
                        setTimeout(function(){
                            $("tr.s-grid-row")[rowid].click();
                            if (parseInt($("#meccancqt").val()) > 1) {
                                $("#meccancqt").val(parseInt($("#meccancqt").val()) - 1);
                                try {
                                    if ($("tr.s-grid-row")[rowid]) {
                                        cancella(rowid);
                                    }
                                }
                                catch (e) {
                                    console.log('finio');
                                }
                            }
                        },50);
                    },500);
                }
                testSiCount+=1;
                if (testSiCount > 50) {
                    console.log("niente da scancellare");
                    clearInterval(testSi);
                }
            },50);
        },500);
    }

}
function test() {
    console.log("func test");
    $(".s-inplace-value-read:contains('Sviluppo')").click();
    $(".s-nav-menu-link[title='Funzione: XXI0090001']")[0].click();
    var testSoc = setInterval(function(){
        if ($("label:contains('Società')").length) {
            clearInterval(testSoc);
            $("label:contains('Società')").next().find("input").trigger('mousedown');
            setTimeout(function(){
                $("label:contains('Società')").next().find("input").val('IT001');
                $("input[type='checkbox'][data-s-selected='false']").click();
                setTimeout(function(){
                    $(".s-mn-main-link[title='OK']")[0].click();
                    setTimeout(function(){
                        flagall();
                    },250);
                },250);
            },250);
        }
    },250);
}
$(document).ready(function() {
    var testDocPronto = setInterval(function() {
        if ($('#s-site-header-middle-cell').length) {
            clearInterval(testDocPronto);
            $("#s-site-header-middle-cell").prepend("<a class='s-site-top-pn-btn' style='margin-right: 8px;background-color: #f57305;display:none;'  id='btnMecHelper' href='#'><div class='s-site-user-name'>Flag</div></a>");
            $("#s-site-header-middle-cell").prepend("<a class='s-site-top-pn-btn' style='margin-right: 8px;background-color: #f57305;display:none;'  id='btnMecTest' href='#'><div class='s-site-user-name'>Salti Attivi</div></a>");
            $("#s-site-header-middle-cell").prepend("<a class='s-site-top-pn-btn' style='margin-right: 8px;background-color: #f57305;padding-right: 9px;display:none;'  id='btnMecCanc' href='#'><div class='s-site-user-name'>DELETE</div><input style='width: 20px;' id='meccancqt' type-'number' value='1'/></a>");
            $('#btnMecHelper').click(function(){aiuti();});
            $('#btnMecTest').click(function(){test();});
            $('#btnMecCanc').click(function(obj){
                if (obj.target.id != 'meccancqt') {
                    cancella(0);
                }
            });
            // cosa mostrare in quale schermata
            setInterval(function(){
                if ($('div.s-page-title:contains("Selezioni per cancellazione")').length > 0) {
                    $('#btnMecHelper').show();
                } else {
                    $('#btnMecHelper').hide();
                }
                if ($(".s-fusion-bar-block-title[title='Fatture terzi fornitore']").length ===1) {
                    $('#btnMecCanc').show();
                } else {
                    $('#btnMecCanc').hide();
                }
                if ($('div:contains("Pagina navigazione")').length > 0) {
                    // menu principale
                    $("#btnMecTest").show();
                } else {
                    $("#btnMecTest").hide();
                }
            },500);
        }
    }, 250);
});
